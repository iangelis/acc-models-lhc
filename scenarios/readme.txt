Structure:

cycle/list.txt: list of cycle names
cycle/<cycle name>/beam_processes.txt: list of beam-processes
cycle/<cycle name>/name.txt: name for the web page
cycle/<cycle name>/description.txt: description for the webpage
cycle/<cycle name>/<beam-process>/<time>/settings.madx: madx file containing settings
cycle/<cycle name>/<beam-process>/<time>/model.madx: madx full madx model
